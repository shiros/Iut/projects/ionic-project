// --------------------------------
// Model : Timer

export class Timer {
  private second: number;
  private minute: number;
  private hour: number;
  private timeSpent: number;

  private started: boolean;
  private wait: boolean;

  constructor() {
    this.second = 0;
    this.minute = 0;
    this.hour = 0;

    this.timeSpent = 0;

    this.started = false;
    this.wait = false;
  }

  // ----------------
  // Timer

  protected launch() {
    setTimeout(() => {
      if(this.started && !this.wait) {
        this.timeSpent++;
        this.prepareTimer(this.timeSpent);
      }

      this.launch();
    }, 1000);
  }

  // ----------------
  // Utils

  public hasStarted() {
    return this.started;
  }

  public hasPaused() {
    return this.wait;
  }

  public start() {
    if (!this.started) {
      this.started = true;
      this.launch();
    }
  }

  public pause() {
    this.wait = true;
  }

  public resume() {
    this.wait = false;
  }

  public reset() {
    this.timeSpent = 0;
    this.prepareTimer(this.timeSpent);
  }

  protected prepareTimer(inputSeconds: number) {
    if(inputSeconds > 0) {
      let sec_num = parseInt(inputSeconds.toString(), 10);

      this.hour = Math.floor(sec_num / 3600);
      this.minute = Math.floor((sec_num - (this.hour * 3600)) / 60);
      this.second = sec_num - (this.hour * 3600) - (this.minute * 60);
    } else {
      this.hour = 0
      this.minute = 0
      this.second = 0
    }
  }

  // ----------------
  // Display

  public display(): string {
    let hoursString = '';
    let minutesString = '';
    let secondsString = '';

    hoursString = (this.hour < 10) ? "0" + this.hour : this.hour.toString();
    minutesString = (this.minute < 10) ? "0" + this.minute : this.minute.toString();
    secondsString = (this.second < 10) ? "0" + this.second : this.second.toString();
    return hoursString + ':' + minutesString + ':' + secondsString;
  }

}
