import { Component } from '@angular/core';

// Entity
import { Rando } from '../../entity/Rando/Rando';

// Services
import { RandoService } from '../../service/Rando/RandoService';

// --------------------------------
// Controller : Home
@Component({
  selector: 'activeRando',
  templateUrl: 'activeRando.html'
})
export class ActiveRando {
  private randoService: RandoService;

  constructor(randoService: RandoService) {
    this.randoService = randoService;
  }

  getRando(): Rando {
    return this.randoService.getRando();
  }

  setActiveRando(): boolean {
    return true;
  }
}
