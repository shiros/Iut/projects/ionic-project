import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AlertController } from 'ionic-angular';

// Controller
import { ListRando } from '../listRando/listRando';

@Component({
  selector: 'home',
  templateUrl: 'home.html'
})
export class HomePage {
  username:string;
  password:string;

  constructor(public navCtrl: NavController,private alertCtrl: AlertController) {}

  logIn() {
    if(this.username == "admin" && this.password == "admin") {
      this.navCtrl.push(ListRando);
    }else{
      let alert = this.alertCtrl.create({
        title: 'Error Username Or PassWord',
        subTitle: 'Try admin/admin :p',
        buttons: ['OK']
      });
      alert.present();
    }
  }
}
