import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Controller
import { DetailPage } from '../detail/detail';

// Entity
import { Rando } from '../../entity/Rando/Rando';

// Repository
import { RandoRepository } from '../../repository/rando/randoRepository';

// Services
import { RandoService } from '../../service/Rando/RandoService';
import { TimerService } from '../../service/Timer/TimerService';

@Component({
  selector: 'listRando',
  templateUrl: 'listRando.html'
})
export class ListRando {
  protected randos: Array<Rando>;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public repository: RandoRepository,
    public randoService: RandoService,
    public timerService: TimerService
  ) {
    this.randos = this.repository.findAll();
  }

  getRandos(): Array<Rando> {
    return this.randos;
  }

  detail(event, item:Rando) {
    this.navCtrl.push(DetailPage, {
      rando: item
    });
  }
}
