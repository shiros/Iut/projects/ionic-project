import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

// Model
import { Rando } from '../../entity/Rando/Rando';

// Service
import { RandoFactory } from '../../service/Rando/RandoFactory';

// --------------------------------
// Service : HttpService
// URL : https://maps.metzmetropole.fr/ows?service=WFS&version=2.0.0&request=GetFeature&typeName=public:pub_tou_bal_nat_rando&srsName=EPSG:4326&outputFormat=json

@Injectable()
export class HttpService {
  protected url: string;
  protected data;

  constructor(public httpClient: HttpClient) {
    //this.url = 'https://maps.metzmetropole.fr/ows?service=WFS&version=2.0.0&request=GetFeature&typeName=public:pub_tou_bal_nat_rando&srsName=EPSG:4326&outputFormat=json';
    this.url = 'https://data.issy.com/api/records/1.0/search/?dataset=plan-departemental-des-itineraires-de-promenade-et-de-randonnee-pdipr&sort=id_section';
    this.data = this.httpClient.get(this.url);
  }

  public fetch(): Array<Rando> {
    let randoList: Array<Rando> = [];

    this.data.subscribe( (data) => {
      data.records.forEach((item => {
        let name = 'Rando n°' + (randoList.length + 1);
        randoList.push(RandoFactory.construct(name, item));
      }));
    });

    return randoList;
  }
}
