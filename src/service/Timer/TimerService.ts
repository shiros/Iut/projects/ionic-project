import { Injectable } from '@angular/core';

// Model
import { Timer } from '../../entity/Timer/Timer';

// --------------------------------
// Service : TimerService

@Injectable()
export class TimerService {
  private timer: Timer;
  private init: boolean;

  constructor() {
    this.init = false;
    this.initTimer();
  }

  // ----------------
  // Timer

  public initTimer(force: boolean = false) {
    if (!this.init || force) {
      this.timer = new Timer();
      this.init = true;
    }
  }

  // ----------------
  // Utils

  public hasStarted(): boolean {
    return this.timer.hasStarted();
  }

  public hasPaused(): boolean  {
    return this.timer.hasPaused();
  }

  public startTimer() {
    this.timer.start();
  }

  public pauseTimer() {
    this.timer.pause();
  }

  public resumeTimer() {
    this.timer.resume();
  }

  public resetTimer() {
    this.timer.reset();
  }

  // ----------------
  // Display

  public getDisplayTime() : String {
    return this.timer.display();
  }
}
